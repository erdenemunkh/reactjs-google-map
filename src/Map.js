import React, { Component } from 'react'
import { Map, GoogleApiWrapper, InfoWindow, Marker, Polygon } from 'google-maps-react';

const mapStyles = {
  width: '100%',
  height: '100%',
};

class MapContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {},
      stores: [{lat: 47.49855629475769, lng: 103.14184416996333},
              {latitude: 47.359423, longitude: 102.021071},
              {latitude: 47.2052192687988, longitude: 103.988426208496},
              {latitude: 47.6307081, longitude: 103.1434325},
              {latitude: 47.3084488, longitude: 103.2140121},
              {latitude: 47.5524695, longitude: 103.0425407}],
      aimags: []
    }
  }
  componentDidMount() {
    fetch('/aimags_alagac_utm.geojson')
    .then(res => res.json())
    .then((resJson) => {
      this.setState({
        aimags: resJson.features
      })
    })
  }
  renderAimags = () => {
    return this.state.aimags.map((aimags, idx) => {
      let coordinates = aimags.geometry.coordinates
      let coordArr = []
      coordinates.map(coordinate => {
        if (coordinate.length > 1) {
          coordinate.map(coords => coordArr.push({lat:coords[1], lng:coords[0]}))
        } else {
          coordinate.map(ub => {
            ub.map(coords => coordArr.push({lat:coords[1], lng:coords[0]}))
          })
        }
      })
      return (
        <Polygon
          key={idx}
          paths={coordArr}
          strokeColor="#0000FF"
          strokeOpacity={0.8}
          strokeWeight={2}
          fillColor="#0000FF"
          fillOpacity={0.35} />
      )
    })
  }
  displayMarkers = () => {
    return this.state.stores.map((store, index) => {
      return <Marker key={index} id={index} position={{
        lat: store.latitude,
        lng: store.longitude
      }}
      icon={{
        url: "/icons8-select-24.png"
      }}
      name={`Position is ${store.latitude}, ${store.longitude}`}
      onClick={this.onMarkerClick} />
    })
  }
  onMarkerClick = (props, marker, e) =>
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });
  render() {
    return (
      <Map
        google={this.props.google}
        zoom={5}
        style={mapStyles}
        initialCenter={{ lat: 47.444, lng: 103.176}}
      >
        {this.displayMarkers()}
        {this.renderAimags()}
        <InfoWindow
          marker={this.state.activeMarker}
          visible={this.state.showingInfoWindow}>
            <div>
              <h3>{this.state.selectedPlace.name}</h3>
            </div>
        </InfoWindow>
      </Map>
    )
  }
}
export default GoogleApiWrapper({
  apiKey: 'AIzaSyDJTAiNeqX67wlugf1EB77j-uHBm-1wpds'
})(MapContainer);